---
# replace "./" with "purplin"
# when you copy this example.md file over
# to your own slidev environment and install
# purplin as a module
theme: purplin
---

# 暑假定向班结课设计CameraX

林东方 计算机学院 计算机科学与技术

<div class="pt-12">
  <span @click="next" class="px-2 p-1 rounded cursor-pointer hover:bg-white hover:bg-opacity-10">
    Press Space for next page <carbon:arrow-right class="inline"/>
  </span>
</div>

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---
layout: image-x
image: 'https://user-images.githubusercontent.com/13499566/138951075-018e65d5-b5fe-4200-9ea7-34315b1764da.jpg'
imageOrder: 1
---

# 基本功能

**加粗**的为已实现的功能

  >拍摄时：
  >
  >- **拥有拍照和录像的预览功能（没有拍摄前有画面）**
  >- **可以拍照，录像（录像包括音频）**
  >- **可以切换后置摄像头与前置摄像头**
  >- **可以通过手指缩放画面**
  >
  >浏览时：
  >
  >- **可以查看已经拍摄的照片和录像**
  >- **图片可以双指缩放**

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: intro
---

# 加分项

**加粗**的为已实现的功能

<br />
<br />

<div class="grid grid-cols-2 gap-x-4">
<div>
  
  >- **拍照和录像时点击屏幕时，调整焦点**
  >- **倒计时拍摄**，连拍功能
  >- 实现滤镜，提供一些风格的滤镜 (iOS可参考GPUImage开源组件)
  >- **亮度较暗时，可以自动或手动开启闪光灯**
  >- 全景拍摄
  >- 感光度(ISO)、快门速度(S)、**曝光补偿(EV)**、白平衡(AWB)等参数调节
  

</div>
<div>

  >- 慢动作摄影
  >- 实现贴纸功能，在照片或录像中添加一些内置的贴纸图片
  >- **可以左右滑动切换拍摄过的照片或录像**
  >- **录像可以快慢进，拖进度条**
  >- 添加剪辑工具主要功能：增加、删除、移动、替换、切片视频片段，尝试增加滤镜效果，增加背景音乐、增加字幕等。实现其中的一种或几种
  >- **创意玩法&功能，提交时附带玩法介绍，期待你的创意！**——**切换网格、手势切换拍照与录像模式、分享功能**

</div>
</div>

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---
layout: intro
---

# 功能展示与源码解析

<div class="pt-12">
  <span @click="next" class="px-2 p-1 rounded cursor-pointer hover:bg-white hover:bg-opacity-10">
    Press Space for next page <carbon:arrow-right class="inline"/>
  </span>
</div>

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

<br />
<br />

---
layout: image-x
image: 'https://tvax2.sinaimg.cn/large/007YIYRqly1h4qrv3cus4j30u01t04cd.jpg'
imageOrder: 1
---

# 拥有拍照和录像的预览功能（没有拍摄前有画面）

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---
layout: image-x
image: 'https://tvax3.sinaimg.cn/large/007YIYRqly1h4qrv34jftj30u01t01ac.jpg)'
imageOrder: 1
---

# 可以拍照，录像（录像包括音频）、此时相机键为红色表示录像状态

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---
layout: image-x
image: 'https://tvax1.sinaimg.cn/large/007YIYRqly1h4qrv3to7fj31001s0dm2.jpg'
imageOrder: 1
---

# 可以切换后置摄像头与前置摄像头

>```kotlin
>@SuppressLint("RestrictedApi")
>fun toggleCamera() = binding.btnSwitchCamera.toggleButton(
>    flag = lensFacing ** CameraSelector.DEFAULT_BACK_CAMERA,
>    rotationAngle = 180f,
>    firstIcon = R.drawable.ic_outline_camera_rear,
>    secondIcon = R.drawable.ic_outline_camera_front,
>) {
>    lensFacing = if (it) {
>        CameraSelector.DEFAULT_BACK_CAMERA //后置摄像头
>    } else {
>        CameraSelector.DEFAULT_FRONT_CAMERA //前置摄像头
>    }
>    startCamera()
>}
>```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---
layout: image-x
image: 'https://tvax1.sinaimg.cn/large/007YIYRqly1h4qv8ded2xj30u01t0am4.jpg'
imageOrder: 1
---

# 拍摄时可以通过手指缩放画面

>```kotlin
>class MyZoomGestureDetector(private var camera: Camera?) : ScaleGestureDetector.SimpleOnScaleGestureListener() {
>        override fun onScale(detector: ScaleGestureDetector): Boolean {
>            val scale = camera?.cameraInfo?.zoomState?.value?.zoomRatio!! * detector.scaleFactor
>            camera!!.cameraControl.setZoomRatio(scale)
>            return super.onScale(detector)
>        }
>}
>```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tvax2.sinaimg.cn/large/007YIYRqly1h4qrv3cus4j30u01t04cd.jpg'
imageOrder: 1
---

# 拥有拍照和录像的预览功能（没有拍摄前有画面）

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tva2.sinaimg.cn/large/007YIYRqly1h4qvf4fzfmj30u01t0afe.jpg'
imageOrder: 1
---

# 可以查看已经拍摄的照片和录像

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tva3.sinaimg.cn/large/007YIYRqly1h4qrv42w38j30u01t0agd.jpg'
imageOrder: 1
---

# 图片可以双指缩放

  >```kotlin
  >class MyScaleGestureDetector(private var imageView:ImageView
  >) : ScaleGestureDetector.SimpleOnScaleGestureListener() {
  >    private var scaleFactor = 1f
  >
  >    override fun onScale(detector: ScaleGestureDetector): Boolean {
  >        scaleFactor *= detector.scaleFactor
  >        scaleFactor = scaleFactor.coerceIn(0.1f, 5.0f)
  >
  >        imageView.scaleX = scaleFactor
  >        imageView.scaleY = scaleFactor
  >
  >        return super.onScale(detector)
  >    }
  >}
  >```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---

# 拍照和录像时点击屏幕时，调整焦点（没画对焦框，就没放图了）

  >```kotlin
  >when(motionEvent.action){
  >    MotionEvent.ACTION_DOWN -> return@setOnTouchListener true
  >    MotionEvent.ACTION_UP -> {
  >        // 从PreviewView获取MeteringPointFactory
  >        val factory = viewFinder.meteringPointFactory
  >
  >        // 从tap坐标创建一个MeteringPoint
  >        val point = factory.createPoint(motionEvent.x, motionEvent.y)
  >
  >        // 从MeteringPoint创建一个MeteringAction，您可以配置它以指定计量模式
  >        val action = FocusMeteringAction.Builder(point).build()
  >
  >        // 触发焦点和计量。由于操作是异步的，该方法返回一个ListenableFuture。当焦点成功或失败时，您可以使用它获得通知。
  >        camera!!.cameraControl.startFocusAndMetering(action)
  >
  >        return@setOnTouchListener true
  >    }
  >```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tvax4.sinaimg.cn/large/007YIYRqly1h4qrv2xfiej30u01t04ay.jpg'
imageOrder: 1
---

# 倒计时拍摄

  >```kotlin
  >binding.btnTimer.setImageResource(
  >    when (timer) {
  >        CameraTimer.S3 -> R.drawable.ic_timer_3 //3秒
  >        CameraTimer.S10 -> R.drawable.ic_timer_10 //10秒
  >        CameraTimer.OFF -> R.drawable.ic_timer_off //无
  >    }
  >)
  >```
  >
  >```kotlin
  >when (selectedTimer) {
  >    CameraTimer.S3 -> for (i in 3 downTo 1) {
  >        binding.tvCountDown.text = i.toString()
  >        delay(1000)
  >    }
  >    CameraTimer.S10 -> for (i in 10 downTo 1) {
  >        binding.tvCountDown.text = i.toString()
  >        delay(1000)
  >    }
  >}
  >```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tva1.sinaimg.cn/large/007YIYRqly1h4qrv3ynr5j30k00zkq7v.jpg'
imageOrder: 1
---

# 亮度较暗时，可以自动或手动开启闪光灯

  >```kotlin
  >binding.llFlashOptions.circularClose(binding.btnFlash) {
  >    flashMode = flash
  >    binding.btnFlash.setImageResource(
  >        when (flash) {
  >            FLASH_MODE_ON -> R.drawable.ic_flash_on //打开
  >            FLASH_MODE_OFF -> R.drawable.ic_flash_off //关闭
  >            else -> R.drawable.ic_flash_auto //自动
  >        }
  >    )
  >    imageCapture?.flashMode = flashMode
  >    prefs.putInt(KEY_FLASH, flashMode)
  >}
  >```
  >
  >```kotlin
  >imageCapture = Builder()
  >                .setCaptureMode(CAPTURE_MODE_MAXIMIZE_QUALITY)
  >                .setFlashMode(flashMode)
  >                .setTargetAspectRatio(aspectRatio)
  >                .setTargetRotation(rotation)
  >                .build()
  >```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tvax2.sinaimg.cn/large/007YIYRqly1h4qrv3ntfbj30u01t0dns.jpg'
imageOrder: 1
---

# 曝光补偿(EV)

  >```kotlin
  >cameraInfo.exposureState.run {
  >        val lower = exposureCompensationRange.lower
  >        val upper = exposureCompensationRange.upper
  >        binding.sliderExposure.run {
  >            valueFrom = lower.toFloat()
  >            valueTo = upper.toFloat()
  >            stepSize = 1f
  >            value = exposureCompensationIndex.toFloat()
  >            addOnChangeListener { _, value, _ ->
  >                cameraControl.setExposureCompensationIndex(value.toInt())
  >            }
  >        }
  >}
  >```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tva2.sinaimg.cn/large/007YIYRqly1h4qrv48yjnj30u01t0n6i.jpg'
imageOrder: 1
---

# 可以左右滑动切换拍摄过的照片或录像

  >```kotlin
  >fun ViewPager2.onPageSelected(action: (Int) -> Unit) {
  >    registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
  >        override fun onPageSelected(position: Int) {
  >            super.onPageSelected(position)
  >            action(position)
  >        }
  >    })
  >}
  >```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tva1.sinaimg.cn/large/007YIYRqly1h4qsyu8up0j30u01t0gyq.jpg'
imageOrder: 1
---

# 显示网格

  >~~~kotlin
  >private fun toggleGrid() {
  >    binding.btnGrid.toggleButton(
  >        flag = hasGrid,
  >        rotationAngle = 180f,
  >        firstIcon = R.drawable.ic_grid_off,
  >        secondIcon = R.drawable.ic_grid_on,
  >    ) { flag ->
  >        hasGrid = flag
  >        prefs.putBoolean(KEY_GRID, flag)
  >        binding.groupGridLines.visibility = if (flag) View.VISIBLE else View.GONE
  >    }
  >}
  >~~~

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---

# 手势切换相机模式与录像模式

  >```kotlin
  >override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
  >    if (e1 ** null || e2 ** null) return super.onFling(e1, e2, velocityX, velocityY)
  >    val deltaX = e1.x - e2.x
  >    val deltaXAbs = abs(deltaX)
  >    if (deltaXAbs >= MIN_SWIPE_DISTANCE_X) {//当“划过的距离"大于“最小的滑动距离”，触发回调函数
  >        if (deltaX > 0) {
  >            swipeCallback?.onLeftSwipe()
  >        } else {
  >            swipeCallback?.onRightSwipe()
  >        }
  >    }
  >    return true
  >}
  >```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: image-x
image: 'https://tvax4.sinaimg.cn/large/007YIYRqly1h4qsluhyjjj30u01t011t.jpg'
imageOrder: 1
---

# 录像可以快慢进，拖进度条（手机自带的）

>```kotlin
>val play = Intent(Intent.ACTION_VIEW, uri).apply { setDataAndType(uri, "video/mp4") }
>startActivity(play)
>```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---
layout: image-x
image: 'https://tvax2.sinaimg.cn/large/007YIYRqly1h4qw841nb3j30u01t0wjq.jpg'
imageOrder: 1
---

# 分享功能

>```kotlin
>fun Fragment.share(media: Media, title: String = "Share with...") {
>    val share = Intent(Intent.ACTION_SEND)
>    share.type = "image/*"
>    share.putExtra(Intent.EXTRA_STREAM, media.uri)
>    startActivity(Intent.createChooser(share, title))
>}
>```

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>

---

# 一感想

<br/>
<br/>

- 开发经验严重不足，对业务逻辑逻辑不清晰、框架不熟悉
- 反反复复地走弯路，g
- 官方文档不是新手能去看的
- 在中文博客里找不到解决方法，就只能去看英文博客了
- 在gitee找个有贴纸功能的App，动辄上万行，一时半会弄不明白，g
- 第一次看到上万行代码的项目、还是很震撼的
- 每做出来一个小功能都会很有成就感

<br>
<br>

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
---
layout: center
class: "text-center"
---

# Learn More

[Documentations](https://sli.dev) / [GitHub Repo](https://github.com/slidevjs/slidev)

<BarBottom  title="Slidev theme purplin">
  <Item text="slidevjs/slidev">
    <carbon:logo-github />
  </Item>
  <Item text="Slidevjs">
    <carbon:logo-twitter />
  </Item>
  <Item text="sli.dev">
    <carbon:link />
  </Item>
</BarBottom>
